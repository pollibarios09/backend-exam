#Installation:

- Run `composer install` to install dependencies.
- Run `php artisan jwt:secret` to generate JWT secret.
- Run `php artisan migrate` to run migration script.
- Run `php artisan serve --port=8000` to start the Backend API.

