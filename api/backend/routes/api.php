<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// guest
Route::post('/login', 'CredentialController@login');
Route::post('/register', 'CredentialController@register');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/logout', 'CredentialController@logout');
});

// Route::post('/posts', 'PostController@store');

Route::apiResource('posts', 'PostController');

Route::apiResource('posts.comments', 'PostCommentController');


// authenticate




