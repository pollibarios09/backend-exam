<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =[
        'body',
        'commentable_type',
        'commentable_id',
        'creator_id',
        'parent_id',
    ];

    /**
     * Get the parent commentable model (post).
     */
    public function commentable()
    {
        return $this->morphTo();
    }


    /**
     * Get the parent commentable model (post).
     */
    public function parentComment()
    {
        return $this->belongsTo('App\Comment', 'id', 'parent_id');
    }

    /**
     * Get the parent commentable model (post).
     */
    public function childComment()
    {
        return $this->hasMany('App\Comment', 'parent_id', 'id');
    }


    /**
     * Get the owner of the post.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }
}
