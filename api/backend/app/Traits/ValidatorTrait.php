<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Illuminate\Validation\ValidationException;

trait ValidatorTrait
{

    /**
     * @param Request $request
     * @param array $validations
     * @return mixed
     */
    public function validate(Request $request, array $validations)
    {

        $validation = Validator::make(
        	$request->all(), $validations
        );

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
    }

}
