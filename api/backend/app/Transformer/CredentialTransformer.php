<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\User;
use App\Traits\TransformerTrait;
use League\Fractal\TransformerAbstract;

class CredentialTransformer extends TransformerAbstract
{
    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
    	return [
    		'id'			=> $user->id,
            'name'          => $user->name,
            'email'         => $user->email,
            'created_at'	=> Carbon::parse($user->created_at)->format('Y-m-d H:i:s'),
    		'updated_at'	=> Carbon::parse($user->updated_at)->format('Y-m-d H:i:s')
    	];
    }
}
