<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{

    /**
     * @param Comment $comment
     * @return array
     */
    public function transform(Comment $comment)
    {
    	return [
    		'id'                => $comment->id,
            'body'              => $comment->body,
            'commentable_type'  => $comment->commentable_type,
            'commentable_id'    => $comment->commentable_id,
            'creator_id'        => $comment->creator_id,
            'parent_id'         => $comment->parent_id,
            'created_at'        => Carbon::parse($comment->created_at)->format('Y-m-d H:i:s'),
    		'updated_at'        => Carbon::parse($comment->updated_at)->format('Y-m-d H:i:s')
    	];
    }



}
