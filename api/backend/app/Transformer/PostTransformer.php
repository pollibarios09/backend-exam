<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Models\Post;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{
    
    /**
     * @param Post $post
     * @return array
     */
    public function transform(Post $post)
    {
    	return [
    		'id'			=> $post->id,
            'user_id'       => $post->user_id,
            'title'         => $post->title,
            'slug'          => $post->slug,
            'created_at'	=> Carbon::parse($post->created_at)->format('Y-m-d H:i:s'),
    		'updated_at'	=> Carbon::parse($post->updated_at)->format('Y-m-d H:i:s')
    	];
    }



}
