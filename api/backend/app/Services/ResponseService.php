<?php

namespace App\Services;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use App\Serializers\DataArraySerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ResponseService
{

	/**
	 * @var Manager
	 */
	private $manager;

	/**
	 * @var IlluminatePaginatorAdapter
	 */
	private $paginator;

	/**
	 * @var mixed | Collection or Item
	 */
	private $resourceFormat;

	/**
	 * Initialize ResponseService
	 */
	public function __construct()
	{
		$this->manager = new Manager;
		$this->manager->setSerializer(new DataArraySerializer());
	}

	/**
	 * @param  mixed               $resource
	 * @param  TransformerAbstract $transformer
	 * @return mixed
	 */
	public function formatCollection($resource, TransformerAbstract $transformer, $resourceKey = '')
	{
		$this->resourceFormat = new Collection(
			$resource, $transformer, $resourceKey
		);

		return $this->manager->createData(
			$this->resourceFormat
		)->toArray();
	}

	/**
	 * @param  mixed $resource
	 * @param  TransformerAbstract $transformer
	 * @param  array $meta
	 * @return mixed
	 */
	public function formatItem($resource, TransformerAbstract $transformer, ?array $meta = [], $resourceKey = '')
	{
		$this->resourceFormat = new Item(
			$resource, $transformer, $resourceKey
		);

		return $this->manager->createData(
			$this->resourceFormat->setMeta($meta)
		)->toArray();
	}

	/**
	 * @param  mixed $resource
	 * @param  TransformerAbstract $transformer
	 * @return mixed
	 */
	public function formatPagination($resource, TransformerAbstract $transformer, $resourceKey = '')
	{
		$this->paginator = $resource;
		$this->resourceFormat = new Collection(
			$resource->getCollection(), $transformer, $resourceKey
		);


		return $this->manager->createData(
			$this->resourceFormat->setPaginator(
				new IlluminatePaginatorAdapter($this->paginator)
			)
		)->toArray();
	}

}
