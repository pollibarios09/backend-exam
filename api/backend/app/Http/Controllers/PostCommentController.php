<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Transformers\CommentTransformer;
use App\Http\Controllers\ResourceController;

class PostCommentController extends ResourceController
{   
    /**
     * @var Comment
     */
    protected $resourceModel;

    /**
     * @var CommentTransformer
     */
    protected $transformer;
    
    /**
     * CredentialController constructor
     */
    public function __construct()
    {
        $this->resourceModel = app(Comment::class);
        $this->transformer = app(CommentTransformer::class);
        $this->middleware('auth')->except(['index', 'show']);

        Parent::__construct();
    }

    /**
     * index function
     *
     * @param Post $post
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function index(Post $post, Request $request) 
    {
        try {
            if (empty($post->comments)) {
                $this->success([]);
            }
            
            return $this->success(
                $this->responseService->formatCollection(
                    $post->comments, $this->transformer
                )
            );
        }
        catch (ModelNotFoundException $error) {
            return $this->error('Something went wrong: '.$error->getMessage());
        }
    }

    /**
     * @param Post $post
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Post $post, Request $request)
    {
        $this->validate($request, [
            'body' => 'required',
        ]);

        $request->request->add([
            'creator_id' => auth()->user()->id,
            'commentable_type'  => Post::class,
            'commentable_id'  => $post->id
        ]);

        return Parent::save($request);
    }

    /**
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'body' => 'required',
        ]);
               
        return Parent::alter($request, $id);
    }


    /**
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function destroy(Request $request, Post $post, Comment $comment)
    {               
        return Parent::remove($comment);
    }
}