<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Transformers\CredentialTransformer;
use App\Http\Controllers\ResourceController;

class CredentialController extends ResourceController
{

    /**
     * @var User
     */
    protected $resourceModel;

    /**
     * @var CredentialTransformer
     */
    protected $transformer;

    /**
     * CredentialController constructor
     */
    public function __construct()
    {
        $this->resourceModel = app(User::class);
        $this->transformer = app(CredentialTransformer::class);
        Parent::__construct();
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        try {
            if ($token = Auth::attempt($request->only('email', 'password'))) {
                return $this->success([
                    'token'         => $token,
                    "token_type"    => "bearer",
                    'expires_in'    => auth()->factory()->getTTL() * 60
                ]);
            }

            throw new Exception('Invalid credentials.', 401);
        } catch (Exception $error) {
            return $this->error('Something went wrong: '.$error->getMessage(), $error->getCode() ? $error->getCode() : 500);
        }
    }


    /**
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request)
    {
        try {
            auth()->logout();

            return $this->success([], 'Token successfully destroyed.');
        } catch (Exception $error) {
            return $this->error('Something went wrong: '.$error->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return $this->unprocessedEntity($validator->errors()->messages(), 422);
        }

        $request['password'] =   Hash::make($request['password']);
        $request['remember_token'] =   Str::random(10);

        return Parent::save($request);
    }
}
