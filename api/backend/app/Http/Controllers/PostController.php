<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\ResponseService;
use App\Transformers\PostTransformer;

class PostController extends ResourceController
{   
    /**
     * @var Post
     */
    protected $resourceModel;

    /**
     * @var PostTransformer
     */
    protected $transformer;

    /**
     * @var ResponseService
     */
    public $responseService;
    
    /**
     * CredentialController constructor
     */
    public function __construct()
    {
        $this->resourceModel = app(Post::class);
        $this->transformer = app(PostTransformer::class);
        $this->middleware('auth')->except(['index', 'show']);
        $this->responseService = app(ResponseService::class);
        Parent::__construct();
    }

    /**
     * show function
     *
     * @param $slug
     * @return Illuminate\Http\Response
     */
    public function show(Post $post) 
    {
        try {
            return $this->success(
                $this->responseService->formatItem(
                    $post, $this->transformer
                )
            );
        }
        catch (ModelNotFoundException $error) {
            return $this->error('Something went wrong: '.$error->getMessage());
        }
    }

    /**
     * index function
     *
     * @param \Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function index(Request $request) 
    {
        try {
            $data = $this->resourceModel->paginate($request->get('limit') > 0 ? (int) $request->get('limit') : 15);

            return $this->success(
                $this->responseService->formatPagination($data, $this->transformer)
            );
        }
        catch (ModelNotFoundException $error) {
            return $this->error('Something went wrong: '.$error->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:posts',
            'content' => 'required',
        ]);

        $request->request->add([
            'slug' => Str::slug($request->get('title'), '-'),
            'user_id' => auth()->user()->id
        ]);
        
        return Parent::save($request);
    }


        /**
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);
        
        if (!empty($request->get('title'))) {
            $request->request->add([
                'slug' => Str::slug($request->get('title'), '-'),
            ]);
        }

        return Parent::alter($request, $post);
    }

    /**
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function destroy(Request $request, Post $post)
    {               
        return Parent::remove($post);
    }

}