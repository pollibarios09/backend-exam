<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ResponseService;
use App\Traits\ResponseFormatterTrait;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ResourceController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ResponseFormatterTrait;


    /**
     * @var ResponseService
     */
    public $responseService;

    /**
     * Controller constructor.
     * @param Resource $resourceService
     * @param ResponseService $responseService
     * @param Request|null $request
     */
    public function __construct() 
    {
        $this->responseService = app(ResponseService::class);
    }

    /**
     * store function
     *
     * @param \Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function save(Request $request) 
    {
        try {
            return $this->success(
                $this->responseService->formatItem(
                    $this->resourceModel->create($request->all()),
                $this->transformer), 
            'Success', 201);
        }
        catch (ModelNotFoundException $error) {
            return $this->error('Something went wrong: '.$error->getMessage());
        }
    }

    /**
     * update function
     *
     * @param \Illuminate\Http\Request $request
     * @param $resource
     * @return Illuminate\Http\Response
     */
    public function alter(Request $request, $resource) {

        try {

            return $this->success(
                $this->responseService->formatItem(
                    tap($resource)->update($request->all())->fresh(),
                $this->transformer), 
            'Successfully updated', 200);
        }
        catch (ModelNotFoundException $error) {
            return $this->error('Something went wrong: '.$error->getMessage());
        }
    }


    /**
     * update function
     *
     * @param \Illuminate\Http\Request $request
     * @param $resource
     * @return Illuminate\Http\Response
     */
    public function remove($resource) {

        try {
            $resource->delete();
            
            return $this->success([], 'Successfully deleted', 200);
        }
        catch (ModelNotFoundException $error) {
            return $this->error('Something went wrong: '.$error->getMessage());
        }
    }
}
